package graph;

import java.util.LinkedList;
import java.util.Iterator;

/**
 * Implementation of graph algorithms for a (undirected) graph structure
 * Considering generic vertex V and edge E types
 *
 * Works on AdjancyMatrixGraph objects
 *
 * @author DEI-ESINF
 *
 */
public class GraphAlgorithms {

    private static <T> LinkedList<T> reverse(LinkedList<T> list) {
        LinkedList<T> reversed = new LinkedList<>();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            reversed.push(it.next());
        }
        return reversed;
    }

    /**
     * Performs depth-first search of the graph starting at vertex. Calls
     * package recursive version of the method.
     *
     * @param <V>
     * @param <E>
     * @param graph Graph object
     * @param vertex Vertex of graph that will be the source of the search
     * @return queue of vertices found by search (including vertex), null if
     * vertex does not exist
     *
     */
    public static <V, E> LinkedList<V> DFS(AdjacencyMatrixGraph<V, E> graph, V vertex) {

        int index = graph.toIndex(vertex);
        if (index == -1) {
            return null;
        }

        LinkedList<V> resultQueue = new LinkedList<>();
        resultQueue.add(vertex);
        boolean[] knownVertices = new boolean[graph.numVertices];
        DFS(graph, index, knownVertices, resultQueue);
        return resultQueue;
    }

    /**
     * Actual depth-first search of the graph starting at vertex. The method
     * adds discovered vertices (including vertex) to the queue of vertices
     *
     * @param graph Graph object
     * @param index Index of vertex of graph that will be the source of the
     * search
     * @param known previously discovered vertices
     * @param verticesQueue queue of vertices found by search
     *
     */
    static <V, E> void DFS(AdjacencyMatrixGraph<V, E> graph, int index, boolean[] knownVertices, LinkedList<V> verticesQueue) {
        knownVertices[index] = true;
        for (int i = 0; i < graph.numVertices; i++) {
            if (graph.edgeMatrix[index][i] != null && knownVertices[i] == false) {
                verticesQueue.add(graph.vertices.get(i));
                DFS(graph, i, knownVertices, verticesQueue);
            }
        }
    }

    /**
     * Performs breath-first search of the graph starting at vertex. The method
     * adds discovered vertices (including vertex) to the queue of vertices
     *
     * @param <V>
     * @param <E>
     * @param graph Graph object
     * @param vertex Vertex of graph that will be the source of the search
     * @return queue of vertices found by search (including vertex), null if
     * vertex does not exist
     *
     */
    public static <V, E> LinkedList<V> BFS(AdjacencyMatrixGraph<V, E> graph, V vertex) {
        int index = graph.toIndex(vertex);
        if (index == -1) {
            return null;
        }
        //Criar duas LinkedList com o mesmo contudo para conseguir trabalhar com uma e manter a outra intacta (em relação aos vertices)
        LinkedList<V> vertexList1 = new LinkedList<>();
        LinkedList<V> vertexList2 = new LinkedList<>();
        //Lista para verificar se os vertices já foram vistos ou não
        boolean[] knownVertices = new boolean[graph.numVertices];
        vertexList2.add(vertex);
        vertexList1.add(vertex);
        knownVertices[index] = true;

        while (!vertexList2.isEmpty()) {
            index = graph.toIndex(vertexList2.getFirst());
            vertexList2.remove(0);
            for (int j = 0; j < graph.numVertices; j++) {
                if (graph.edgeMatrix[index][j] != null && knownVertices[j] == false) {
                    vertexList1.add(graph.vertices.get(j));
                    vertexList2.add(graph.vertices.get(j));
                    knownVertices[j] = true;
                }
            }
        }
        return vertexList1;
    }

    /**
     * All paths between two vertices Calls recursive version of the method.
     *
     * @param <V>
     * @param <E>
     * @param graph Graph object
     * @param source Source vertex of path
     * @param dest Destination vertex of path
     * @param paths
     * @return false if vertices not in the graph
     *
     */
    public static <V, E> boolean allPaths(AdjacencyMatrixGraph<V, E> graph, V source, V dest, LinkedList<LinkedList<V>> paths) {
        int sourceId = graph.toIndex(source);
        if (sourceId == -1) {
            return false;
        }

        int destId = graph.toIndex(dest);
        if (destId == -1) {
            return false;
        }

        paths.clear();

        LinkedList<V> stackAux = new LinkedList<>();
        boolean[] knownVertices = new boolean[graph.numVertices];
        allPaths(graph, sourceId, destId, knownVertices, stackAux, paths);
        return true;
    }

    /**
     * Actual paths search The method adds vertices to the current path (stack
     * of vertices) when destination is found, the current path is saved to the
     * list of paths
     *
     * @param graph Graph object
     * @param sourceIdx Index of source vertex
     * @param destIdx Index of destination vertex
     * @param knownVertices previously discovered vertices
     * @param auxStack stack of vertices in the path
     * @param path LinkedList with paths (queues)
     *
     */
    static <V, E> void allPaths(AdjacencyMatrixGraph<V, E> graph, int sourceIdx, int destIdx, boolean[] knownVertices, LinkedList<V> auxStack, LinkedList<LinkedList<V>> paths) {
        auxStack.push(graph.vertices.get(sourceIdx));
        knownVertices[sourceIdx] = true;
        for (int i = 0; i < graph.numVertices; i++) {
            if (graph.edgeMatrix[sourceIdx][i] != null) {
                if (i == destIdx) {
                    auxStack.push(graph.vertices.get(i));
                    paths.add(reverse(auxStack));
                    auxStack.pop();
                } else {
                    if (knownVertices[i] == false) {
                        allPaths(graph, i, destIdx, knownVertices, auxStack, paths);
                    }
                }
            }
        }
        knownVertices[sourceIdx] = false;
        auxStack.pop();
    }

    /**
     * Transforms a graph into its transitive closure uses the Floyd-Warshall
     * algorithm
     *
     * @param <V>
     * @param <E>
     * @param graph Graph object
     * @param dummyEdge object to insert in the newly created edges
     * @return the new graph
     */
    public static <V, E> AdjacencyMatrixGraph<V, E> transitiveClosure(AdjacencyMatrixGraph<V, E> graph, E dummyEdge) {

        AdjacencyMatrixGraph<V, E> newGraph = (AdjacencyMatrixGraph<V, E>) graph.clone();
        for (int k = 0; k < newGraph.numVertices; k++) {
            for (int i = 0; i < newGraph.numVertices; i++) {
                if (i != k && newGraph.edgeMatrix[i][k] != null) {
                    for (int j = 0; j < newGraph.numVertices; j++) {
                        if (i != j && k != j && newGraph.edgeMatrix[k][j] != null) {
                            newGraph.insertEdge(newGraph.vertices.get(i), newGraph.vertices.get(j), dummyEdge);
                        }
                    }
                }
            }
        }
        return newGraph;
    }
}

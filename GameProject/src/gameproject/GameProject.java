/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gameproject;

import graph.AdjacencyMatrixGraph;
import graph.EdgeAsDoubleGraphAlgorithms;
import static graph.GraphAlgorithms.allPaths;
import graphbase.GraphAlgorithmsMap;
import graphbase.Graph;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Random;

/**
 *
 * @author João Pires <your.name at your.org>
 */
public class GameProject {

    private static AdjacencyMatrixGraph<Local, Double> matrixGraph;

    private static Graph<Personagem, Boolean> adjacencyMap;

    public GameProject() {
        matrixGraph = new AdjacencyMatrixGraph();
        adjacencyMap = new Graph(false);
    }

    public AdjacencyMatrixGraph<Local, Double> getMatrixGraph() {
        return matrixGraph;
    }

    public void setMatrixGraph(AdjacencyMatrixGraph<Local, Double> matrixGraph) {
        this.matrixGraph = matrixGraph;
    }

    public Graph<Personagem, Boolean> getAdjacencyMap() {
        return adjacencyMap;
    }

    public void setAdjacencyMap(Graph<Personagem, Boolean> adjacencyMap) {
        this.adjacencyMap = adjacencyMap;
    }

    //Alinea 1.b)
    public double B1_getCaminhoMenorDificuldade(Local origem, Local destino, LinkedList<Local> path) {
        //    System.out.println(matrixGraph.toString());
        return EdgeAsDoubleGraphAlgorithms.shortestPath(matrixGraph, origem, destino, path);
    }

    //Alinea 1.c) 
    public LinkedList<Local> C1_verificarConquista(Personagem p, Local local) {
        ArrayList<Local> locaisPersonagem = new ArrayList<>();
        LinkedList<LinkedList<Local>> caminhosPossiveis = new LinkedList<>();
        LinkedList<Local> locaisIntermedios = new LinkedList<>();
        double forcaDistanciaMinima = Double.MAX_VALUE;

        System.out.println(p.getNomePersonagem() + " vs " + local.getDonoLocal().getNomePersonagem());

        for (Local l : matrixGraph.vertices()) {
            if (l.getDonoLocal().getNomePersonagem().equals(p.getNomePersonagem())) {
                locaisPersonagem.add(l);
            }
        }

        for (Local l : locaisPersonagem) {
            allPaths(matrixGraph, l, local, caminhosPossiveis);
            for (int i = 0; i < caminhosPossiveis.size(); i++) {
                LinkedList<Local> path = caminhosPossiveis.get(i);
                double forcaConquista = 0;
                for (int j = 0; j < path.size() - 1; j++) {
                    if (path.get(j + 1).getDonoLocal() == p) {
                        forcaConquista += matrixGraph.getEdge(path.get(j), path.get(j + 1));
                    } else {
                        forcaConquista += path.get(j + 1).getDonoLocal().getPontosPersonagem() + path.get(j + 1).getPontosLocal() + matrixGraph.getEdge(path.get(j), path.get(j + 1));
                    }
                }
                if (forcaConquista <= forcaDistanciaMinima) {
                    forcaDistanciaMinima = forcaConquista;
                    locaisIntermedios = path;
                }
            }
        }

        if (forcaDistanciaMinima <= p.getPontosPersonagem()) {
            System.out.println("A personagem " + p.getNomePersonagem() + " pontos: " + p.getPontosPersonagem() + " pode conquistar o local " + local.getNomeLocal());
            System.out.println("Dificuldade: " + forcaDistanciaMinima);
        } else {
            System.out.println("A personagem " + p.getNomePersonagem() + " pontos: " + p.getPontosPersonagem() + " não pode conquistar o local " + local.getNomeLocal());
            System.out.println("Dificuldade: " + forcaDistanciaMinima);
        }
        return locaisIntermedios;
    }

//Alinea 2.b)
    public Iterable<Personagem> B2_getAliados(Personagem p1) {
        ArrayList<Personagem> aliados = new ArrayList<>();

        for (Personagem p : adjacencyMap.vertices()) {
            if (adjacencyMap.getEdge(p, p1) != null) {
                aliados.add(p);
            }
        }
        return aliados;
    }

//    //Alinea 2.c) Joao
//    public Iterable<Object> C2_aliancaMaisForte() {
//        ArrayList<Object> aliancaForte = new ArrayList<>();
//        double maxForca = Double.MIN_VALUE;
//        Personagem p1 = new Personagem();
//        Personagem p2 = new Personagem();
//
//        for (Edge<Personagem, Boolean> edge : adjacencyMap.edges()) {
//            if (edge.getVDest().getPontosPersonagem() + edge.getVOrig().getPontosPersonagem() * edge.getWeight() > maxForca) {
//                maxForca = edge.getVDest().getPontosPersonagem() + edge.getVOrig().getPontosPersonagem() * edge.getWeight();
//                p1 = edge.getVOrig();
//                p2 = edge.getVDest();
//            }
//        }
//
//        aliancaForte.add(maxForca);
//        aliancaForte.add(p1);
//        aliancaForte.add(p2);
//
//        return aliancaForte;
//    }
    //Alínea 2.c) 
    public double C2_getAliancaMaisForte(LinkedList<Personagem> aliados) {
        double forcaMax = Double.MIN_VALUE;
        for (Personagem p1 : adjacencyMap.vertices()) {
            for (Personagem p2 : adjacencyMap.vertices()) {
                if (adjacencyMap.getEdge(p1, p2) != null && ((double) (p1.getPontosPersonagem() + p2.getPontosPersonagem()) * adjacencyMap.getEdge(p1, p2).getWeight()) >= forcaMax) {
                    aliados.clear();
                    forcaMax = ((p1.getPontosPersonagem() + p2.getPontosPersonagem()) * adjacencyMap.getEdge(p1, p2).getWeight());
                    aliados.add(p1);
                    aliados.add(p2);
                }
            }
        }
        return forcaMax;
    }

    //Alinea 2.d)
    public void D2_criarAlianca(Personagem p1, Personagem p2) {
        LinkedList<Personagem> redeAlianca = this.D2_getAliancaMenorCaminho(p1, p2);

        if (redeAlianca.size() > 0) {
            adjacencyMap.insertEdge(p1, p2, true, D2_calcularFatorMedio(redeAlianca));
        } else {
            adjacencyMap.insertEdge(p1, p2, true, D2_fatorCompatibilidadeAleatorio());
        }
    }

    //Alinea 2.d)
    public double D2_fatorCompatibilidadeAleatorio() {
        Random rand = new Random();
        int flag = rand.nextInt(3);
        double fator = 0.0;
        switch (flag) {
            case 0:
                fator = 0.2;
                break;
            case 1:
                fator = 0.5;
                break;
            case 2:
                fator = 0.8;
                break;
            default:
                break;
        }
        return fator;
    }

    //Alinea 2.d)
    public float D2_calcularFatorMedio(LinkedList<Personagem> shortPath) {

        float totalFatorCompatibilidade = 0;

        for (int i = 0; i < shortPath.size() - 1; i++) {
            totalFatorCompatibilidade += adjacencyMap.getEdge(shortPath.get(i), shortPath.get(i + 1)).getWeight();
        }

        return (float) (totalFatorCompatibilidade / (shortPath.size() - 1));
    }

    //Alinea 2.d)
    public LinkedList<Personagem> D2_getAliancaMenorCaminho(Personagem p1, Personagem p2) {
        ArrayList<LinkedList<Personagem>> caminhosPossiveis = new ArrayList<>();
        LinkedList<Personagem> menorCaminho = new LinkedList<>();
        double minNumAliancas = Double.MAX_VALUE;

        caminhosPossiveis = GraphAlgorithmsMap.allPaths(adjacencyMap, p1, p2);
        for (int i = 0; i < caminhosPossiveis.size(); i++) {
            LinkedList<Personagem> path = caminhosPossiveis.get(i);
            double numAliancas = 0;
            for (int j = 0; j < path.size(); j++) {
                numAliancas++;
            }
            if (numAliancas <= minNumAliancas) {
                minNumAliancas = numAliancas;
                menorCaminho = path;
            }
        }
        return menorCaminho;
    }

    //Alinea 2.e)
    public Graph<Personagem, Boolean> E2_grafoAliancasPublicas() {

        Graph<Personagem, Boolean> graphAliancas = new Graph<>(false);

        for (Personagem p1 : adjacencyMap.vertices()) {
            for (Personagem p2 : adjacencyMap.vertices()) {
                if (adjacencyMap.getEdge(p1, p2) == null && p1 != p2) {
                    if (!graphAliancas.validVertex(p1)) {
                        graphAliancas.insertVertex(p1);
                    }
                    if (!graphAliancas.validVertex(p2)) {
                        graphAliancas.insertVertex(p2);
                    }
                    graphAliancas.insertEdge(p1, p2, Boolean.TRUE, D2_fatorCompatibilidadeAleatorio());
                }
            }
        }

        return graphAliancas;
    }

    //Alinea 3.f)
    public controloJogo F3_aliancaConquista(Personagem p, Local local, Personagem aliado) {
        LinkedList<LinkedList<Local>> caminhosPossiveis = new LinkedList<>();
        LinkedList<Local> locaisIntermedios = new LinkedList<>();
        double forcaDistanciaMinima = Double.MAX_VALUE;

        Local l = this.F3_getLocalPersonagem(p);

        double forcaAlianca = p.getPontosPersonagem();
        if (adjacencyMap.getEdge(p, aliado) != null && local.getDonoLocal() != aliado) {
            forcaAlianca += aliado.getPontosPersonagem();
        } else {
            throw new IllegalArgumentException("Não é possivel conquistar local!");
        }

        allPaths(matrixGraph, l, local, caminhosPossiveis);
        for (int i = 0; i < caminhosPossiveis.size(); i++) {
            LinkedList<Local> path = caminhosPossiveis.get(i);
            double forcaConquista = 0;
            for (int j = 0; j < path.size() - 1; j++) {
                if (path.get(j + 1).getDonoLocal() != aliado) {
                    if (path.get(j + 1).getDonoLocal() == p) {
                        forcaConquista += matrixGraph.getEdge(path.get(j), path.get(j + 1));
                    } else {
                        forcaConquista += path.get(j + 1).getDonoLocal().getPontosPersonagem() + path.get(j + 1).getPontosLocal() + matrixGraph.getEdge(path.get(j), path.get(j + 1));
                    }
                } else {
                    break;
                }
            }
            if (forcaConquista <= forcaDistanciaMinima) {
                forcaDistanciaMinima = forcaConquista;
                locaisIntermedios = path;
            }
        }

        controloJogo ret = new controloJogo(aliado, forcaDistanciaMinima, locaisIntermedios);

        if (forcaDistanciaMinima <= forcaAlianca) {
            System.out.println("A personagem " + p.getNomePersonagem() + " pontos aliança: " + forcaAlianca + " pode conquistar o local " + local.getNomeLocal());
            System.out.println("Dificuldade: " + forcaDistanciaMinima);
        } else {
            System.out.println("A personagem " + p.getNomePersonagem() + " pontos aliança: " + forcaAlianca + " não pode conquistar o local " + local.getNomeLocal());
            System.out.println("Dificuldade: " + forcaDistanciaMinima);
        }

        return ret;
    }

    //Alinea 3.f)
    public Local F3_getLocalPersonagem(Personagem p) {
        Local l = new Local();

        for (Local aux : matrixGraph.vertices()) {
            if (aux.getNomeLocal().equals(p.getLocalInicial())) {
                l = aux;
            }
        }
        return l;
    }

    class controloJogo {

        private Personagem aliado;
        private double forcaNecessaria;
        private LinkedList<Local> locaisConquistar;

        public controloJogo(Personagem aliado, double forcaNecessaria, LinkedList<Local> locaisConquistar) {
            this.aliado = aliado;
            this.forcaNecessaria = forcaNecessaria;
            this.locaisConquistar = locaisConquistar;
        }

        public Personagem getAliado() {
            return aliado;
        }

        public void setAliado(Personagem aliado) {
            this.aliado = aliado;
        }

        public double getForcaNecessaria() {
            return forcaNecessaria;
        }

        public void setForcaNecessaria(double forcaNecessaria) {
            this.forcaNecessaria = forcaNecessaria;
        }

        public LinkedList<Local> getLocaisConquistar() {
            return locaisConquistar;
        }

        public void setLocaisConquistar(LinkedList<Local> locaisConquistar) {
            this.locaisConquistar = locaisConquistar;
        }
        

    }

    public Local getLocal(Integer posicao) {
        ArrayList<Local> vertices = (ArrayList<Local>) matrixGraph.vertices();

        if (posicao >= matrixGraph.numVertices() || posicao < 0) {
            throw new IllegalArgumentException("Local não existe!");
        }

        Local l = vertices.get(posicao);
        return l;
    }

    public Personagem getPersonagem(Integer posicao) {
        int i = 0;

        if (posicao >= adjacencyMap.numVertices() || posicao < 0) {
            throw new IllegalArgumentException("Local não existe!");
        }

        for (Personagem p : adjacencyMap.vertices()) {
            if (i == posicao) {
                return p;
            }
            i++;
        }

        return null;
    }
}

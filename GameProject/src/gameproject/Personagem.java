/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gameproject;

/**
 *
 * @author João Pires <your.name at your.org>
 */
public class Personagem {

    private String nomePersonagem;
    private double pontosPersonagem;
    private String localInicial;

    private static final String NOME_PERSONAGEM_POR_OMISSAO = "";
    private static final double PONTOS_PERSONAGEM_POR_OMISSAO = 0;
    private static final String LOCAL_INICIAL_POR_OMISSAO = "";

    public Personagem(String nome, double pontos, String local) {
        nomePersonagem = nome;
        pontosPersonagem = pontos;
        localInicial = local;
    }

    public Personagem() {
        nomePersonagem = NOME_PERSONAGEM_POR_OMISSAO;
        pontosPersonagem = PONTOS_PERSONAGEM_POR_OMISSAO;
        localInicial = LOCAL_INICIAL_POR_OMISSAO;
    }

    /**
     * @return the nomePersonagem
     */
    public String getNomePersonagem() {
        return nomePersonagem;
    }

    /**
     * @return the pontosPersonagem
     */
    public double getPontosPersonagem() {
        return pontosPersonagem;
    }

    /**
     * @param nomePersonagem the nomePersonagem to set
     */
    public void setNomePersonagem(String nomePersonagem) {
        this.nomePersonagem = nomePersonagem;
    }

    /**
     * @param pontosPersonagem the pontosPersonagem to set
     */
    public void setPontosPersonagem(double pontosPersonagem) {
        this.pontosPersonagem = pontosPersonagem;
    }

    public String getLocalInicial() {
        return localInicial;
    }

    public void setLocalInicial(String localInicial) {
        this.localInicial = localInicial;
    }

    @Override
    public String toString() {
        return "Personagem{" + "nomePersonagem=" + nomePersonagem + ", pontosPersonagem=" + pontosPersonagem + ", localInicial=" + localInicial + '}';
    }
    
}

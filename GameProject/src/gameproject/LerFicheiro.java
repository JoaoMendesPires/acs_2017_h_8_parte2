/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gameproject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author João Pires <your.name at your.org>
 */
public class LerFicheiro {

    GameProject proj = new GameProject();

    //Alinea 1.a)
    public void A1_lerFicheiroLocais(String fileName) throws FileNotFoundException {
        ArrayList<Local> listaLocais = new ArrayList();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String linha;
            while ((linha = reader.readLine()) != null) {
                String[] tmp = linha.split(",");
                if (tmp.length == 2) {
                    Local local = new Local(tmp[0], Double.parseDouble(tmp[1]));
                    proj.getMatrixGraph().insertVertex(local);
                    listaLocais.add(local);
                }
                if (tmp.length == 3) {
                    for (Local l : listaLocais) {
                        for (Local l2 : listaLocais) {
                            if (l.getNomeLocal().equals(tmp[0]) && l2.getNomeLocal().equals(tmp[1])) {
                                proj.getMatrixGraph().insertEdge(l, l2, Double.parseDouble(tmp[2]));
                            }
                        }
                    }
                }
            }
            reader.close();
        } catch (Exception e) {
            System.err.format("Exception occurred trying to read '%s'.", fileName);
            e.printStackTrace();
        }
    }

    //Alinea 2.a)
    public void A2_lerFicheiroPers(String fileName) throws FileNotFoundException {
        ArrayList<Personagem> listaPersonagens = new ArrayList();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String linha;
            while ((linha = reader.readLine()) != null) {
                String[] tmp = linha.split(",");
                if (tmp.length == 3) {
                    Personagem personagem = new Personagem(tmp[0], Double.parseDouble(tmp[1]), tmp[2]);
                    proj.getAdjacencyMap().insertVertex(personagem);
                    listaPersonagens.add(personagem);
                    for (Local l : proj.getMatrixGraph().vertices()) {
                        if (tmp[2].equals(l.getNomeLocal())) {
                            l.setDonoLocal(personagem);
                        }
                    }
                }
                if (tmp.length == 4) {
                    for (Personagem p1 : listaPersonagens) {
                        for (Personagem p2 : listaPersonagens) {
                            if (p1.getNomePersonagem().equals(tmp[0]) && p2.getNomePersonagem().equals(tmp[1])) {
                                proj.getAdjacencyMap().insertEdge(p1, p2, Boolean.parseBoolean(tmp[2]), Double.parseDouble(tmp[3]));
                            }
                        }
                    }
                }
            }
            //A2_atribuirPersonagensLocais(listaPersonagens);
            reader.close();
        } catch (Exception e) {
            System.err.format("Exception occurred trying to read '%s'.", fileName);
            e.printStackTrace();
        }
    }

//    //Atribuição aleatória de personagens a locais
//    public void A2_atribuirPersonagensLocais(ArrayList<Personagem> listaPersonagens) {
//        for (Local l : proj.getMatrixGraph().vertices()) {
//            Random rand = new Random();     
//            int localVazio = rand.nextInt(3) + 0;
//            int num = rand.nextInt(listaPersonagens.size()) + 0;
//            /*A variável local vazio serve para estabelecer que aproximadamente 
//            1/4 dos locais (se localVazio = 0) não terão dono*/
//            if (listaPersonagens.get(num) != null & localVazio > 0) {
//                l.setDonoLocal(listaPersonagens.get(num));
//            }
//        }
//    }
}

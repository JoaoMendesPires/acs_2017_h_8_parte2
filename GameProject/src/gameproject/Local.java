/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gameproject;

/**
 *
 * @author João Pires <your.name at your.org>
 */
public class Local {

    private String nomeLocal;
    private double pontosLocal;
    private Personagem donoLocal;

    private static final String NOME_LOCAL_POR_OMISSAO = "";
    private static final double PONTOS_LOCAL_POR_OMISSAO = 0;

    public Local(String nome, double pontos) {
        nomeLocal = nome;
        pontosLocal = pontos;
        donoLocal = new Personagem();
    }

    public Local() {
        nomeLocal = NOME_LOCAL_POR_OMISSAO;
        pontosLocal = PONTOS_LOCAL_POR_OMISSAO;
        donoLocal = new Personagem();
    }

    /**
     * @return the nomeLocal
     */
    public String getNomeLocal() {
        return nomeLocal;
    }

    /**
     * @return the pontosLocal
     */
    public double getPontosLocal() {
        return pontosLocal;
    }

    /**
     * @param nomeLocal the nomeLocal to set
     */
    public void setNomeLocal(String nomeLocal) {
        this.nomeLocal = nomeLocal;
    }

    /**
     * @param pontosLocal the pontosLocal to set
     */
    public void setPontosLocal(double pontosLocal) {
        this.pontosLocal = pontosLocal;
    }

    public Personagem getDonoLocal() {
        return donoLocal;
    }

    public void setDonoLocal(Personagem donoLocal) {
        this.donoLocal = donoLocal;
    }

}

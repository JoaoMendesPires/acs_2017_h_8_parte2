/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gameproject;

import gameproject.GameProject.controloJogo;
import graph.AdjacencyMatrixGraph;
import graphbase.Graph;
import java.io.FileNotFoundException;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author Henrique Maciel <1150863@isep.ipp.pt>
 */
public class F3_aliancaConquistaTest {

    GameProject projeto = new GameProject();
    LerFicheiro leitura = new LerFicheiro();

    @Test
    public void testF3_aliancaConquista() throws FileNotFoundException {
        leitura.A1_lerFicheiroLocais("Locais_Caminhos.txt");
        leitura.A2_lerFicheiroPers("Personagens_Aliancas.txt");

        Local l = projeto.getLocal(1);
        Personagem p = projeto.getPersonagem(7);
        Personagem a = projeto.getPersonagem(8);
        controloJogo cj = projeto.F3_aliancaConquista(p, l, a);
        assertTrue("Aliado deve ser válido", projeto.getAdjacencyMap().validVertex(cj.getAliado()));
    }
}

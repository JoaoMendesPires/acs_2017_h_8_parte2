/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gameproject;

import graphbase.Graph;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Henrique Maciel <1150863@isep.ipp.pt>
 */
public class GameProjectTest {

    GameProject projeto = new GameProject();
    LerFicheiro leitura = new LerFicheiro();
    String fichLocais = "locais_S.txt";
    String fichPers = "pers_S.txt";

    public GameProjectTest() {
    }

    @Test
    public void testB1_CaminhoMenorDificuldade() throws FileNotFoundException {
        leitura.A1_lerFicheiroLocais(fichLocais);
        LinkedList<Local> path = new LinkedList<>();
        double res = projeto.B1_getCaminhoMenorDificuldade(projeto.getLocal(0), projeto.getLocal(2), path);
        System.out.println("Caminho menor dificuldade: ");
        for (int i = 0; i < path.size(); i++) {
            System.out.println(path.get(i).getNomeLocal());
        }
        System.out.println("");
        assertTrue("Valor de path deveria ser maior que 0", res > 0);
        assertTrue("Path deve estar preenchido", path.size() > 0);
    }

    @Test
    public void testB2_getAliados() throws FileNotFoundException {
        leitura.A2_lerFicheiroPers(fichPers);
        System.out.println("Lista aliados: ");
        for (Personagem p : projeto.B2_getAliados(projeto.getPersonagem(0))) {
            System.out.println(p.getNomePersonagem());
            assertTrue("Aliados devem ser personagens validas", projeto.getAdjacencyMap().validVertex(p));
        }
        System.out.println("");

    }

    @Test
    public void testD2_criarAlianca() throws FileNotFoundException {
        leitura.A2_lerFicheiroPers(fichPers);
        int numAliancasOriginal = projeto.getAdjacencyMap().numEdges();
        Personagem p1 = projeto.getPersonagem(0);
        Personagem p2 = projeto.getPersonagem(1);
        Personagem p3 = projeto.getPersonagem(7);
        projeto.D2_criarAlianca(p1, p2);
        projeto.D2_criarAlianca(p1, p3);
        int numAliancasNovo = projeto.getAdjacencyMap().numEdges();
        System.out.println(projeto.getAdjacencyMap().toString());
        assertTrue("Devem haver 2 novas alianças", numAliancasNovo > numAliancasOriginal);
    }

    @Test
    public void testC2_GetAliancaMaisForte() throws FileNotFoundException {
        leitura.A2_lerFicheiroPers(fichPers);
        LinkedList<Personagem> aliados = new LinkedList<>();
        double maiorForca = projeto.C2_getAliancaMaisForte(aliados);
        System.out.println("Aliança mais poderosa: ");
        for (Personagem p : aliados) {
            System.out.println(p.getNomePersonagem());
        }
        System.out.println("Força combinada: " + maiorForca + "\n");
        assertTrue("Força devia ser maior que (ou igual) a 0", maiorForca >= 0);

    }

    @Test
    public void testE2_GrafoAliancasPublicas() throws FileNotFoundException {
        leitura.A2_lerFicheiroPers(fichPers);
        Graph<Personagem, Boolean> graphAliancas = projeto.E2_grafoAliancasPublicas();
        System.out.println(graphAliancas.toString());
        assertTrue("graphAliancas deve estar preenchido", graphAliancas.numEdges() > 0);
        assertTrue("graphAliancas deve estar preenchido", graphAliancas.numVertices() > 0);
    }

    @Test
    public void testC1_verificarConquista() throws FileNotFoundException {
        leitura.A1_lerFicheiroLocais("Locais_Caminhos.txt");
        leitura.A2_lerFicheiroPers("Personagens_Aliancas.txt");

        Local l = projeto.getLocal(2);
        Personagem p = projeto.getPersonagem(7);

        for (Local local : projeto.C1_verificarConquista(p, l)) {
            assertTrue("Locais intermédios devem ser válidos", projeto.getMatrixGraph().checkVertex(local));
        }
    }
}

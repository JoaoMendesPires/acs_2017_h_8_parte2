/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gameproject;

import graph.AdjacencyMatrixGraph;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Henrique Maciel <1150863@isep.ipp.pt>
 */
public class LerFicheiroTest {

    LerFicheiro leitura = new LerFicheiro();
    GameProject projeto = new GameProject();
    String fichLocais = "locais_S.txt";
    String fichPers = "pers_S.txt";

    @Test
    public void testLerFicheiroLocais() throws FileNotFoundException {
        leitura.A1_lerFicheiroLocais(fichLocais);
        System.out.println(projeto.getMatrixGraph().toString());
        assertTrue("Matriz de adjacencias deve conter caminhos", projeto.getMatrixGraph().numEdges() != 0);
        assertTrue("Matriz de adjacencias deve conter locais", projeto.getMatrixGraph().numVertices() != 0);
    }

    @Test
    public void testLerFicheiroAliancas() throws FileNotFoundException {
        leitura.A2_lerFicheiroPers(fichPers);
        System.out.println(projeto.getAdjacencyMap().toString());
        for (Local l : projeto.getMatrixGraph().vertices()) {
            System.out.println(l.getDonoLocal().toString());
        }
        assertTrue("Mapa de adjacencias deve conter caminhos", projeto.getAdjacencyMap().numEdges() != 0);
        assertTrue("Mapa de adjacencias deve conter locais", projeto.getAdjacencyMap().numVertices() != 0);
    }
}
